# OpenPDAC-11

[![SQAaaS badge shields.io](https://img.shields.io/badge/sqaaas%20software-silver-silver)](https://eu.badgr.com/public/assertions/s7DDg9SvRX6_IN8mUnkbuw "SQAaaS silver badge achieved")
[![GitLab Release (latest by SemVer)](https://img.shields.io/github/v/release/demichie/OpenPDAC-11)](https://github.com/demichie/OpenPDAC-11/releases)
[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.10721203.svg)](https://doi.org/10.5281/zenodo.10721203)

OpenPDAC is an offline Eulerian-Lagrangian model for the simulation of volcanic fluids. The model solves the
conservation equations of mass, momentum and energy for a Eulerian-Eulerian compressible multiphase mixture of
gasses and fine (up to mm size) particles, and the Lagrangian transport equations for coarser solid particles. The solver is
based on the equations of the PDAC model, ported and optimized on the open-source OpenFOAM C++ toolbox for
Computational Fluid Dynamics (CFD).

## Documentation

## Developers

## License & Copyright

  GNU GENERAL PUBLIC LICENSE
                       Version 3, 29 June 2007

 Copyright (C) 2007 Free Software Foundation, Inc. <https://fsf.org/>
 Everyone is permitted to copy and distribute verbatim copies
 of this license document, but changing it is not allowed.
